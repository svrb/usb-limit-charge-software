#!/usr/bin/python

# general:
# setEnabled()
# .clicked.connect
# 
# file, dir:
# file,_ = QtWidgets.QFileDialog.getOpenFileName(None,"text", "default file name","file type")
# file,_ = QtWidgets.QFileDialog.getSaveFileName(None,"text", "default file name","file type")
# dir  = QtWidgets.QFileDialog.getExistingDirectory(None,"text","")
# 
# text edit:
# .toPlainText
# .toHtml
# .setText
# setHtml
# setPlainText
# 
# line:
# setText
# text
# textChanged(text)
# 
# spin box: 
# value()
# setValue()
# valueChanged() #signal
# 
# 
# combo box:
# .currentTextChanged.connect
# .currentText()
# addItems([])
# 
# radio box:
# toggled() #signal
# isChecked()
# setChecked(True)
# 
# inputdialog:
# QtWidgets.QInputDialog.getItem(parent, title, label, items_list ,0,False) #default=0, editable=False
# getDouble (parent, title, label, value=0, minValue=-2147483647, maxValue=2147483647, decimals=1])
# getInt (parent, title, label[, value=0, minValue=-2147483647, maxValue=2147483647, step=1])
# getMultiLineText (parent, title, label[, text=””])
# getText (parent, title, label[, echo=QLineEdit.Normal, text=””])
# 
# table: 
# .setHorizontalHeaderLabels(("name"))
# .clearContents()
# .rowCount()
# .setRowCount()
# .removeRow()
# .QtWidgets.QTableWidgetItem()
# .setItem(row.col,item)
# .item(row,col)
# .cellChanged.connect(row,col) # signal for change content
# .cellActivated.connect(row,col) # signal for select cell
# 
# message box:
# QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical,"title","text body")

from PyQt5.QtWidgets import QDialog, QApplication, QTableWidgetItem ,QCompleter
from PyQt5 import QtCore , QtWidgets , QtGui
import sys
import ui
import serial
import time,os
import serial.tools.list_ports
import threading
import var
import subprocess
class main_ui(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = ui.Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.refresh_btn.clicked.connect(self.list_port)
        self.list_port()
        self.ui.start_btn.clicked.connect(self.start)
        self.ui.stop_btn.clicked.connect(self.stop)
        self.ui.force_stop_btn.clicked.connect(self.force_stop)
        self.ui.force_start_btn.clicked.connect(self.force_start)
        
        self.tray = QtWidgets.QSystemTrayIcon(self)
        self.tray.setIcon(QtGui.QIcon("./icon.png"))
        #tray.setVisible(True)
        menu = QtWidgets.QMenu()
        show_win_item = QtWidgets.QAction("show",self)
        hide_win_item = QtWidgets.QAction("hide",self)
        quit = QtWidgets.QAction("Quit",self)
        menu.addAction(show_win_item)
        menu.addAction(hide_win_item)
        menu.addAction(quit)
        show_win_item.triggered.connect(self.show)
        hide_win_item.triggered.connect(self.hide)
        quit.triggered.connect(app.quit)
        self.tray.setContextMenu(menu)
        self.tray.show()
        self.tray.activated.connect(self.tray_click) # when click on icon
        
        self.tray.setToolTip("usb limit charge")
        
    def tray_click(self,reason):
        if reason == QtWidgets.QSystemTrayIcon.Trigger: # when click on tray icon (code 3)
            self.show()
        
        
    def closeEvent(self, event):
        event.ignore()
        self.hide()
        
        
    def force_start(self):
        port = self.ui.port_list.currentText()
        try:
            var.serial.close()
        except:
            pass
        #var.serial = serial.Serial(port)
        #var.serial.setRTS(False)
        
    def force_stop(self):
        port = self.ui.port_list.currentText()
        var.serial = serial.Serial(port)
        var.serial.setRTS(True)
        
    def list_port (self):
        self.ui.port_list.clear()
        for i in list(serial.tools.list_ports.comports()):
            self.ui.port_list.addItem(i.device)
    def stop (self):
        var.run_thr = False
        self.ui.start_btn.setEnabled(True)
        self.ui.stop_btn.setEnabled(False)
        
    def start(self):
        var.run_thr = True
        self.ui.stop_btn.setEnabled(True)
        self.ui.start_btn.setEnabled(False)
        var.port = self.ui.port_list.currentText()
        var.serial = serial.Serial(var.port)
        var.low_limit = self.ui.low_limit.value()
        var.up_limit = self.ui.up_limit.value()
        threading.Thread(target=thr.run , daemon=True).start()
            
class thread():
    def run(self):
        while var.run_thr:
            battery_power = subprocess.run("acpi",stdout = subprocess.PIPE,universal_newlines=True).stdout.strip().split()
            precent = [i for i in battery_power if i.find('%')!=-1][0].split('%')[0]
            precent = int(precent)
            if precent >= var.up_limit:
                var.serial = serial.Serial(var.port)
                var.serial.setRTS(True)
                main.ui.status.setText("دشارژ")
                
            if precent <= var.low_limit:
                #var.serial.setRTS(False)
                main.ui.status.setText("شارژ")
                var.serial.close()
            time.sleep(10)




thr = thread()


app = QApplication(sys.argv)
main = main_ui()
main.show()
sys.exit(app.exec_())


